using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace TheGame
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        Bus[] v = new Bus[3];
        Vector3 BusPosition = new Vector3(7f, 0, -9);
        Model BusModel;
        int whichBusDidHit = 0;
        Quaternion BusRotation = Quaternion.Identity;

        BoundingSphere heroFrontSphere;
        KeyboardState oldkeyboardstate = new KeyboardState();
        int cameraStateCounter = 0;
        DebugDraw g;
        Matrix worldMatrix;
        BoundingSphere heroBackSpere;
        int BrakingFlag;
        enum CollisionType { None, Building, Boundary, Target, HeroCar }
        Quaternion cameraRotation = Quaternion.Identity;

        float moveSpeed = (float)0;
        int IsCarStillInside = 0;
        Texture2D[] skyboxTextures;
        Vector3 heroPosition = new Vector3(7.5f, 0, -3);
        Vector3 heroFrontSpherePosition = new Vector3(0f, 0f, -0.4f);
        Quaternion heroRotation = Quaternion.Identity;

        float xCamera = 0f;
        float yCamera = 0.12f;
        float zCamera = 0.6f;
        Model lancer;
        FloorMapGenerator map = new FloorMapGenerator();

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GraphicsDevice device;
        Effect effect;
        Effect effect2;
        Vector3 lightDirection = new Vector3(3, -2, 5);
        Matrix viewMatrix;
        Matrix projectionMatrix;
        Texture2D texture;
        Texture2D sceneryTexture;
        Texture2D ass;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }


        private void UpdateCamera()
        {

            cameraRotation = Quaternion.Lerp(cameraRotation, heroRotation, 0.1f);

            Vector3 campos = new Vector3(xCamera, yCamera, zCamera);
            campos = Vector3.Transform(campos, Matrix.CreateFromQuaternion(cameraRotation));
            campos += heroPosition;

            Vector3 camup = new Vector3(0, 1, 0);
            camup = Vector3.Transform(camup, Matrix.CreateFromQuaternion(cameraRotation));

            viewMatrix = Matrix.CreateLookAt(campos, heroPosition, camup);
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, device.Viewport.AspectRatio, 0.2f, 500.0f);

        }


        private CollisionType CheckCollision(BoundingSphere sphere)
        {
            for (int i = 0; i < map.buildingBoundingBoxes.Length; i++)
                if (map.buildingBoundingBoxes[i].Contains(sphere) != ContainmentType.Disjoint)
                    return CollisionType.Building;

            return CollisionType.None;
        }

        private CollisionType CheckCollision2(BoundingSphere sphere)
        {
            for (whichBusDidHit = 0; whichBusDidHit < 3; whichBusDidHit++)
            {
                if (sphere.Contains(v[whichBusDidHit].busBodyCenter) != ContainmentType.Disjoint || sphere.Contains(v[whichBusDidHit].busBodyFront) != ContainmentType.Disjoint || sphere.Contains(v[whichBusDidHit].busBodyBack) != ContainmentType.Disjoint)
                {

                    return CollisionType.HeroCar;
                }

            }


            return CollisionType.None;
        }


        private void RenderHero()
        {

            worldMatrix = Matrix.CreateScale(0.03f, 0.03f, 0.03f) * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateFromQuaternion(heroRotation) * Matrix.CreateTranslation(heroPosition);
            Matrix[] LancerTransforms = new Matrix[lancer.Bones.Count];
            for (int ibones = 46; ibones < 64; ibones++)
                lancer.Bones[ibones].Transform = Matrix.CreateRotationY(moveSpeed / 1) * lancer.Bones[ibones].Transform;

            lancer.CopyAbsoluteBoneTransformsTo(LancerTransforms);
            foreach (ModelMesh mesh in lancer.Meshes)
            {

                if (mesh.Name != "BLightLG" && mesh.Name != "BLightRG" && mesh.Name != "BrakeRL" && mesh.Name != "WindR")
                {
                    foreach (BasicEffect renderingEffect in mesh.Effects)
                    {
                        renderingEffect.LightingEnabled = true;
                        renderingEffect.DirectionalLight0.DiffuseColor = new Vector3(1f, 1, 1);
                        renderingEffect.DirectionalLight0.Direction = new Vector3(1, 0, 0);
                        renderingEffect.AmbientLightColor = new Vector3(0.2f, 0.2f, 0.2f);

                        renderingEffect.World = LancerTransforms[mesh.ParentBone.Index] * worldMatrix;
                        if (mesh.Name == "BLightL" || mesh.Name == "BLightR")
                        {
                            if (BrakingFlag == 1)
                            {
                                renderingEffect.DirectionalLight0.DiffuseColor = new Vector3(114f, 0, 0);
                                renderingEffect.AmbientLightColor = new Vector3(0.2f, 0.2f, 0.2f);
                                renderingEffect.EmissiveColor = new Vector3(1, 0, 0);
                            }
                            else
                            {
                                renderingEffect.LightingEnabled = false;
                            }
                        }
                        renderingEffect.View = viewMatrix;
                        renderingEffect.Projection = projectionMatrix;
                    }

                    mesh.Draw();
                }
            }


        }

        private void RenderBus()
        {
            for (int whichBus = 0; whichBus < 3; whichBus++)
            {
                worldMatrix = Matrix.CreateScale(0.024f, 0.024f, 0.024f) * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateFromQuaternion(v[whichBus].busRotation) * Matrix.CreateTranslation(v[whichBus].busPosition);
                Matrix[] BusTransforms = new Matrix[BusModel.Bones.Count];

                BusModel.CopyAbsoluteBoneTransformsTo(BusTransforms);

                foreach (ModelMesh mesh in BusModel.Meshes)
                {
                    foreach (BasicEffect effec in mesh.Effects)
                    {
                        effec.EnableDefaultLighting();
                        effec.LightingEnabled = true;
                        effect.Parameters["xAmbient"].SetValue(0.3f);
                        effec.World = BusTransforms[mesh.ParentBone.Index] * worldMatrix;
                        effec.View = viewMatrix;
                        effec.Projection = projectionMatrix;
                    }
                    mesh.Draw();
                }
            }
        }

        private void DrawModel()
        {

            Matrix worldMatrix = Matrix.CreateScale(0.03f, 0.03f, 0.03f) * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateFromQuaternion(heroRotation) * Matrix.CreateTranslation(heroPosition);
            Matrix[] LancerTransforms = new Matrix[lancer.Bones.Count];
            lancer.CopyAbsoluteBoneTransformsTo(LancerTransforms);
            int i = 0;

            foreach (ModelMesh mesh in lancer.Meshes)
            {
                foreach (Effect currentEffect in mesh.Effects)
                {
                    currentEffect.CurrentTechnique = currentEffect.Techniques["Textured"];
                    currentEffect.Parameters["xWorld"].SetValue(LancerTransforms[mesh.ParentBone.Index] * worldMatrix);
                    currentEffect.Parameters["xView"].SetValue(viewMatrix);
                    currentEffect.Parameters["xProjection"].SetValue(projectionMatrix);
                    currentEffect.Parameters["xTexture"].SetValue(skyboxTextures[i++]);
                }
                mesh.Draw();
            }

        }

        private void DrawCity()
        {

            effect.Parameters["xEnableLighting"].SetValue(true);
            effect.Parameters["xLightDirection"].SetValue(lightDirection);
            effect.Parameters["xAmbient"].SetValue(0.3f);
            effect.CurrentTechnique = effect.Techniques["Textured"];
            effect.Parameters["xWorld"].SetValue(Matrix.Identity);
            effect.Parameters["xView"].SetValue(viewMatrix);
            effect.Parameters["xProjection"].SetValue(projectionMatrix);
            effect.Parameters["xTexture"].SetValue(sceneryTexture);

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.SetVertexBuffer(map.cityVertexBuffer);
                device.DrawPrimitives(PrimitiveType.TriangleList, 0, map.cityVertexBuffer.VertexCount / 3);
            }
        }

        protected override void Initialize()
        {
            g = new DebugDraw(GraphicsDevice);
            lightDirection.Normalize();
            graphics.PreferredBackBufferWidth = 900;
            graphics.PreferredBackBufferHeight = 900;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();
            Window.Title = "TheGame";

            base.Initialize();
        }

        protected override void LoadContent()
        {
            RasterizerState rasterizerState = new RasterizerState();
            rasterizerState.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rasterizerState;

            spriteBatch = new SpriteBatch(GraphicsDevice);

            for (int whichBus = 0; whichBus < 3; whichBus++)
            {
                v[whichBus] = new Bus();
                v[whichBus].SetBusPosition(new Vector3(2.5f, 0, -30 - whichBus));
            }

            device = graphics.GraphicsDevice;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            texture = Content.Load<Texture2D>("riemerstexture");
            sceneryTexture = Content.Load<Texture2D>("texturemap");
            map.LoadFloorPlan(device);
            ass = Content.Load<Texture2D>("as");
            effect = Content.Load<Effect>("effects");
            effect2 = Content.Load<Effect>("effects2");

            lancer = Content.Load<Model>("Lancer\\LANCEREVOX");
            BusModel = Content.Load<Model>("VW\\Volkswagen");
            SetUpCamera();
        }

        private void SetUpCamera()
        {
            viewMatrix = Matrix.CreateLookAt(new Vector3(xCamera, yCamera, zCamera), new Vector3(8, 0, -7), new Vector3(0, 1, 0));
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, device.Viewport.AspectRatio, 0.2f, 500.0f);
        }

        private void ProcessKeyboard(GameTime gameTime)
        {
            float leftRightRot = 0;

            float turningSpeed = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;
            turningSpeed *= 1.0f * 1;
            KeyboardState keys = Keyboard.GetState();
            if (keys.IsKeyDown(Keys.Right))
                leftRightRot -= turningSpeed;
            if (keys.IsKeyDown(Keys.Left))
                leftRightRot += turningSpeed;

            Quaternion additionalRot = Quaternion.CreateFromAxisAngle(new Vector3(0, 1, 0), leftRightRot);
            heroRotation *= additionalRot;



        }

        private void MoveForward(ref Vector3 position, Quaternion rotationQuat, float speed, ref Vector3 positionf)
        {
            Vector3 addVector = Vector3.Transform(new Vector3(0, 0, -1), rotationQuat);
            Vector3 addVector2 = Vector3.Transform(new Vector3(0, 0, -1), rotationQuat);
            position += addVector * speed;
        }
        private void BusCollisionDetection(BoundingSphere front, BoundingSphere left, BoundingSphere right, int whichBus)
        {
            if (v[whichBus].busStopMoveFlag == 1)
            {
                v[whichBus].busMoveSpeed = -0.01f;
                v[whichBus].busStopMoveFlag = 0;
            }

            if (CheckCollision(v[whichBus].busFrontRadar) != CollisionType.None)
            {
                v[whichBus].rotateBusFlag = 1;
            }
            if (CheckCollision(v[whichBus].busLeftRadar) != CollisionType.None && v[whichBus].amountOfBusRotation == 0)
            {
                v[whichBus].busLeftDirectionContainSomething = 1;
            }
            else if (v[whichBus].amountOfBusRotation == 0)
            {
                v[whichBus].busLeftDirectionContainSomething = 0;
            }

            if (CheckCollision(v[whichBus].busRightRadar) != CollisionType.None && v[whichBus].amountOfBusRotation == 0)
            {
                v[whichBus].busRightDirectionContainSomething = 1;

            }
            else if (v[whichBus].amountOfBusRotation == 0)
            {
                v[whichBus].busRightDirectionContainSomething = 0;
            }

            if (CheckCollision2(heroBackSpere) != CollisionType.None)
            {
                v[whichBusDidHit].busMoveSpeed = 0;
                v[whichBusDidHit].busStopMoveFlag = 1;
            }


        }




        protected override void Update(GameTime gameTime)
        {
            BrakingFlag = 0;
            for (int whichBus = 0; whichBus < 3; whichBus++)
            {
                if (v[whichBus].busMoveSpeed > -0.01)
                    v[whichBus].busMoveSpeed -= (float)0.0001;
            }
            Vector3 vector = new Vector3(0, 0, (float)-0.1);
            Vector3 vector2 = new Vector3((float)-0.1, 0, 0);
            Vector3 vectorup = new Vector3(0, (float)0.4, 0);
            if (moveSpeed > 0)
                ProcessKeyboard(gameTime);

            MoveForward(ref heroPosition, heroRotation, moveSpeed, ref heroFrontSpherePosition);

            for (int whichBus = 0; whichBus < 3; whichBus++)
            {
                v[whichBus].RotateBus(gameTime);
                MoveForward(ref v[whichBus].busPosition, v[whichBus].busRotation, v[whichBus].busMoveSpeed, ref heroFrontSpherePosition);
            }
            KeyboardState keyboardstate = Keyboard.GetState();
            if (keyboardstate.IsKeyDown(Keys.Up))
            {
                moveSpeed += (float)0.001;
            }
            if (keyboardstate.IsKeyDown(Keys.Down))
            {
                moveSpeed -= (float)0.001;
                BrakingFlag = 1;

            }

            if (keyboardstate.IsKeyDown(Keys.U))
            {

                Quaternion additionalRot = Quaternion.CreateFromAxisAngle(new Vector3(0, 1, 0), 0.2f);
                cameraRotation *= additionalRot;

            }
            if (keyboardstate.IsKeyDown(Keys.J))
            {
                heroPosition = heroPosition - vectorup;

            }

            if (keyboardstate.IsKeyDown(Keys.C) && oldkeyboardstate.IsKeyUp(Keys.C))
            {
                if (cameraStateCounter > 1)
                {
                    cameraStateCounter = 0;
                }

                if (cameraStateCounter == 0)
                {
                    yCamera += 9f;

                }

                if (cameraStateCounter == 1)
                {
                    yCamera -= 9f;

                }

                cameraStateCounter++;



            }

            oldkeyboardstate = Keyboard.GetState();
            Matrix positionRotationMatrix = Matrix.CreateTranslation(-heroPosition) *
                                   Matrix.CreateFromQuaternion(heroRotation) *
                                   Matrix.CreateTranslation(heroPosition);



            Vector3 translation = Vector3.Transform(heroFrontSpherePosition + heroPosition,
                                           positionRotationMatrix);


            UpdateCamera();
            heroBackSpere = new BoundingSphere(heroPosition, 0.1f);
            heroFrontSphere = new BoundingSphere(translation, 0.2f);


            if (CheckCollision(heroBackSpere) != CollisionType.None || CheckCollision2(heroBackSpere) != CollisionType.None)
            {
                if (IsCarStillInside == 0)
                    moveSpeed = -moveSpeed / 2;

                if (IsCarStillInside > 1)
                {
                    moveSpeed = -moveSpeed;

                }
                if (IsCarStillInside > 2)
                {
                    moveSpeed = moveSpeed * 1.3f;
                }

                IsCarStillInside += 1;

            }
            else
                IsCarStillInside = 0;

            for (int whichBus = 0; whichBus < 3; whichBus++)
            {
                v[whichBus].BusSpheresMove(ref v[whichBus].busFrontRadar, ref v[whichBus].busLeftRadar, ref v[whichBus].busRightRadar);
                BusCollisionDetection(v[whichBus].busFrontRadar, v[whichBus].busLeftRadar, v[whichBus].busRightRadar, whichBus);
            }
            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            device.Clear(Color.White);
            DrawCity();
            RenderHero();
            RenderBus();
            base.Draw(gameTime);
        }
    }
}


