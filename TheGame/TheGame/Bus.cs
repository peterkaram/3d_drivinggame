﻿//// Bus Structure
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace TheGame
{
    public class Bus
    {
     public int busLeftDirectionContainSomething = 0;
     public int busRightDirectionContainSomething = 0;
     public float busMoveSpeed = 0;
     public Vector3 busPosition;

     public BoundingSphere busFrontRadar;
     public BoundingSphere busLeftRadar;
     public BoundingSphere busRightRadar;
     public BoundingSphere busBodyFront;
     public BoundingSphere busBodyCenter;
     public BoundingSphere busBodyBack;

     public Vector3 busFrontRadarPosition = new Vector3(0, 0, 1.10f);
     public Vector3 busLeftRadarPosition = new Vector3(1.2f, 0, 0.3f); 
     public Vector3 busRightRadarPosition = new Vector3(-1.2f, 0, 0.3f);

     public Vector3 busBodyCenterPosition = new Vector3(0, 0, 0.05f);
     public Vector3 busBodyFrontPosition = new Vector3(0, 0, 0.19f);
     public Vector3 busBodyBackPosition = new Vector3(0, 0, -0.11f);

     public Quaternion busRotation = Quaternion.Identity;
     public int rotateBusFlag = 0;
     public float amountOfBusRotation;
     public int busStopMoveFlag = 0;

        public void SetBusPosition(Vector3 position)
        {            
         busPosition = position;   
        }

        public void BusSpheresMove(ref BoundingSphere front, ref BoundingSphere left, ref BoundingSphere right)
        {
            Vector3 translation;
            translation = PoseBoundingSpheres(busPosition, busRotation, busFrontRadarPosition);
            busFrontRadar = new BoundingSphere(translation, 0.1f);
            
            translation = PoseBoundingSpheres(busPosition, busRotation, busLeftRadarPosition);
            busLeftRadar= new BoundingSphere(translation, 0.1f);

            translation = PoseBoundingSpheres(busPosition, busRotation, busRightRadarPosition);
            busRightRadar = new BoundingSphere(translation, 0.1f);
            
            translation = PoseBoundingSpheres(busPosition, busRotation, busBodyCenterPosition);
            busBodyCenter = new BoundingSphere(translation, 0.08f);

            translation = PoseBoundingSpheres(busPosition, busRotation, busBodyFrontPosition);
            busBodyFront = new BoundingSphere(translation, 0.08f);

            translation = PoseBoundingSpheres(busPosition, busRotation, busBodyBackPosition);
            busBodyBack = new BoundingSphere(translation, 0.08f);
        }

        private Vector3 PoseBoundingSpheres(Vector3 Position, Quaternion Rotation, Vector3 ShiftedPosition)
        {
            Matrix positionRotationMatrix = Matrix.CreateTranslation(-Position) *
                                   Matrix.CreateFromQuaternion(Rotation) *
                                   Matrix.CreateTranslation(Position);

            Vector3 translation = Vector3.Transform(ShiftedPosition + Position,
                                           positionRotationMatrix);
            return translation;
        }

        public void RotateBus(GameTime gameTime)
        {
                float leftRightRot = 0;
                float turningSpeed = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000.0f;

                if (rotateBusFlag == 1 && busStopMoveFlag == 0)
                {
                    amountOfBusRotation += 1;
                    if (busRightDirectionContainSomething == 0)
                    {
                        leftRightRot -= turningSpeed + 0.000043f;
                    }
                    else if (busLeftDirectionContainSomething == 0)
                    {
                        leftRightRot += turningSpeed + 0.000043f;
                    }
                    if (busLeftDirectionContainSomething == 1 && busRightDirectionContainSomething == 1)
                    {
                        busStopMoveFlag = 1;
                        busMoveSpeed = 0;
                    }    
                }
                if (amountOfBusRotation > 93f)
                {
                    rotateBusFlag = 0;
                    amountOfBusRotation = 0;
                }
                Quaternion additionalRot = Quaternion.CreateFromAxisAngle(new Vector3(0, 1, 0), leftRightRot);
                busRotation *= additionalRot;
            }
        }
    }
