
Open world 3D racing game implemented on XNA using c#. 


* Free roam driving game inspired by the Egyptian streets
* Built using Visual Studio C# Express 2010, XNA 4.0 
* Date: Third quarter, 2014 

### Guidelines ###

* For Running the game, open: TheGame / TheGame / bin / x86 / Debug / TheGame.exe
* Keys: Directional Keys, C: Change Camera
* To run the code: Visual Studio 2010,XNA 4.0 are needed 

### Testing ###

* Tested with adding up to 6 roaming vehicles in the same scene: No major performance issues.

### Screenshots ###

![TheGame_2016_10_13_14_26_13_066.jpg](https://bitbucket.org/repo/7dK75L/images/1514368138-TheGame_2016_10_13_14_26_13_066.jpg)
![TheGame_2016_10_13_14_06_24_427.jpg](https://bitbucket.org/repo/7dK75L/images/587855714-TheGame_2016_10_13_14_06_24_427.jpg)![TheGame_2016_10_13_14_07_46_319.jpg](https://bitbucket.org/repo/7dK75L/images/1652457733-TheGame_2016_10_13_14_07_46_319.jpg)!